package solver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import dictionary.DictionaryPrep;
import dictionary.DictionaryPrep.DictionaryItem;

public class InteractiveSolver {
	private static int suggestionCount = 5;
	
	private static String[] positions;
	private static Set<String> containedLetters;
	private static List<DictionaryItem> dictionaryItems;
	
	public static void main(String[] args) throws IOException {
		for (String arg : args) {
			if (arg.toLowerCase().contains("-c")) {
				suggestionCount = Integer.parseInt(arg.substring(2));
			}
		}
		initializePattern();
		runSolver();
	}
	
	private static void runSolver() throws IOException {
		Scanner scan = new Scanner(System.in);
		
		printInstructions();
		while(true) {
			System.out.println("=====================================");
			System.out.print("Enter guess or command: ");
			String guess = scan.nextLine();

			if ("x".equals(guess.toLowerCase())) break;
			
			if ("n".equals(guess.toLowerCase())) {
				initializePattern();
				System.out.println();
				printInstructions();
				continue;
			}
			
			if ("?".equals(guess)) {
				showPatterns();
				continue;
			}
			
			if ("d".equals(guess)) {
				// Set the remaining dictionary word scores
				DictionaryPrep.updateLetterScoreMap(dictionaryItems);
				DictionaryPrep.updateDictionaryScores(dictionaryItems);
				dictionaryItems.sort(null);
				Collections.reverse(dictionaryItems);
				
				DictionaryPrep.printLetterScores();
				System.out.println("Dictionary Size: " + dictionaryItems.size());
				continue;
			}
			
			if ("d+".equals(guess)) {
				// Set the remaining dictionary word scores
				DictionaryPrep.updateLetterScoreMap(dictionaryItems);
				DictionaryPrep.updateDictionaryScores(dictionaryItems);
				dictionaryItems.sort(null);
				Collections.reverse(dictionaryItems);
				
				DictionaryPrep.printLetterScores();
				System.out.println("Dictionary Size: " + dictionaryItems.size());
				
				int counter = 0;
				for (int i = dictionaryItems.size() - 1; i >= 0; i--) {
					System.out.println(dictionaryItems.get(i));
					
					if (counter++ == 25) break;
				}
				System.out.println();
				continue;
			}
			
			if ("r".equals(guess.toLowerCase())) {
				System.out.print("Enter word to remove: ");
				String removedWord = scan.nextLine();
				
				removeWord(removedWord);
				
				System.out.print(removedWord + " removed\n\n");
				continue;
			}
			
			System.out.print("Enter result: ");
			String result = scan.nextLine();
			
			System.out.println("Suggestion(s): " + getSuggestionString(getPatterns(guess, result)) + "\n\n");
		}
		scan.close();
	}

	private static void printInstructions() {
		System.out.println("=====================================");
		System.out.println("guess: The word used in the wordle");
		System.out.println("result: The pattern for the word as...");
		System.out.println("\t0 - miss");
		System.out.println("\t1 - contains, misplaced");
		System.out.println("\t2 - contains, correctly placed");
		System.out.println("Enter \"x\" in guess to end the program");
		System.out.println("Enter \"n\" in guess to restart the program");
		System.out.println("Enter \"?\" in guess to see patterns used");
		System.out.println();
		
		// Cannot write to a jar file
		System.out.println("Enter \"r\" in guess to collect word that should be removed from the dictionary");

	}

	private static void removeWord(String removedWord) throws IOException {
		FileOutputStream os = new FileOutputStream(new File("src/removed.txt"), true);
		os.write((removedWord + "\n").getBytes());
		os.close();
	}
	
	public static String getSuggestionString(List<Pattern> patterns) throws IOException {
		return Arrays.toString(getSuggestions(patterns).toArray());
	}

	public static List<String> getSuggestions(List<Pattern> patterns) throws IOException {
		if (dictionaryItems == null) {
			dictionaryItems = DictionaryPrep.getDictionaryItems();
		}
		
		List<String> suggestions = new LinkedList<String>();
		for (int i = dictionaryItems.size() - 1; i >= 0; i--) {
			String word = dictionaryItems.get(i).getWord();
			Boolean matches = true;
			for (Pattern p : patterns) {
				if (!p.matcher(word).matches()) {
					matches = false;
					break;
				}
			}
			
			if (!matches) {
				dictionaryItems.remove(i);
			}
		}
		
		// Set the remaining dictionary word scores
		DictionaryPrep.updateLetterScoreMap(dictionaryItems);
		DictionaryPrep.updateDictionaryScores(dictionaryItems);
		dictionaryItems.sort(null);
		Collections.reverse(dictionaryItems);
		
		for (int i = dictionaryItems.size() - 1; i >= 0; i--) {
			suggestions.add(dictionaryItems.get(i).getWord());
			
			if (suggestions.size() == suggestionCount) break;
		}
		
		return suggestions;
	}
	
	public static void initializePattern() throws IOException {
		dictionaryItems = DictionaryPrep.getDictionaryItems();

		// Set the dictionary word scores
		DictionaryPrep.updateLetterScoreMap(dictionaryItems);
		DictionaryPrep.updateDictionaryScores(dictionaryItems);
		dictionaryItems.sort(null);
		Collections.reverse(dictionaryItems);
		
		positions = new String[5];
		positions[0] = positions[1] = positions[2] = positions[3] = positions[4] = "abcdefghijklmnopqrstuvwxyz";
		
		containedLetters = new HashSet<>();
	}
	
	public static List<Pattern> getPatterns(String guess, String result) {
		List<Pattern> patterns = new LinkedList<Pattern>();
		
		for (int i = 0; i < 5; i++) {
			if (positions[i].length() == 1) continue;
			
			String p = result.substring(i, i + 1);
			String g = guess.substring(i, i + 1);
			switch(p) {
				case "0":
				case "n":
					if (containedLetters.contains(g)) continue;
					
					for (int j = 0; j < 5; j++) {
						positions[j] = positions[j].replace(g, "");
					}
					break;
				case "1":
				case "c":
					positions[i] = positions[i].replace(g, "");
					containedLetters.add(g);
					break;
				case "2":
				case "y":
					positions[i] = g;
					containedLetters.add(g);
					break;
			}
		}
		
		patterns.add(Pattern.compile("["
				+ positions[0] + "]["
				+ positions[1] + "]["
				+ positions[2] + "]["
				+ positions[3] + "]["
				+ positions[4] + "]"));
		
		for (String s : containedLetters) {
			patterns.add(Pattern.compile("(.)*([" + s + "])(.)*"));
		}

		return patterns;
	}
	
	public static void showPatterns() {
		List<Pattern> patterns = new LinkedList<Pattern>();
		
		patterns.add(Pattern.compile("["
				+ positions[0] + "]["
				+ positions[1] + "]["
				+ positions[2] + "]["
				+ positions[3] + "]["
				+ positions[4] + "]"));
		
		for (String s : containedLetters) {
			patterns.add(Pattern.compile("(.)*([" + s + "])(.)*"));
		}
		

		for (Pattern p : patterns) {
			System.out.println(p.toString());
		}
	}
}
