package automated;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import dictionary.DictionaryPrep;
import solver.InteractiveSolver;

public class WordleRunner {
	public static Metrics metrics;
	public static void main(String[] args) throws IOException {
		int runCount = 10;
		String fileName = null;
		String startingGuess = "adieu";
		for (String arg : args) {
			if (arg.contains("file=")) {
				fileName = arg.split("=")[1];
				continue;
			}
			
			if (arg.contains("guess=")) {
				startingGuess = arg.split("=")[1];
				continue;
			}
			
			if (Pattern.compile("[\\d]+").matcher(arg).matches()) {
				runCount = Integer.parseInt(arg);
			}
		}
		
		metrics = new Metrics();
		for (int i = 0; i < runCount; i++) {
			runWordle(startingGuess);
		}
		
		if (fileName != null) {
			FileOutputStream os = new FileOutputStream(new File(fileName));
			os.write(metrics.toString().getBytes());
			os.close();
		} else {
			System.out.println(metrics);
		}
	}

	public static void runWordle(String startingGuess) throws IOException {
		List<String> wordList = DictionaryPrep.getWords();
		String wordle = wordList.get((int)(Math.random() * wordList.size()));
		
		InteractiveSolver.initializePattern();
		
		String currentGuess = startingGuess;
		Integer guessCount = 1;
		metrics.initRun(wordle);
		for (int i = 0; i < 7; i++) {
			String result = getResult(wordle, currentGuess);
			metrics.incrementRun(currentGuess, result);
			if ("22222".equals(result)) {
				metrics.updateMetric(guessCount);
				break;
			}
			
			guessCount++;
			List<String> suggestions = InteractiveSolver.getSuggestions(InteractiveSolver.getPatterns(currentGuess, result));
			if (suggestions.size() > 0) {
				currentGuess = suggestions.get(0);
			}
			
			if (i == 6) {
				metrics.expireRun();
			}
		}
	}
	
	public static String getResult(String wordle, String guess) {
		String result = "";
		Map<String, Integer> checkedCount = new HashMap<>();
		for (int j = 0; j < 5; j++) {
			String l = guess.substring(j, j+1);
			String w = wordle.substring(j, j+1);
			
			if (l.equals(w)) {
				result += "2";
				
				Integer count = checkedCount.get(l);
				if (count == null) {
					count = 0;
				}
				count++;
				checkedCount.put(l, count);
			} else if (wordle.contains(l)) {
				Integer count = checkedCount.get(l);
				if (count == null) {
					count = 0;
				}
				count++;
				checkedCount.put(l, count);
				
				int start = 0;
				Boolean valid = true;
				for (int k = 0; k < count; k++) {
					if (start >= 5) break;
					
					int fo = wordle.indexOf(l, start);
					if (fo > -1) {
						start = fo + 1;
					}
					
					if (fo == -1) {
						valid = false;
						break;
					}
				}
				
				result += valid ? "1" : "0";
			} else {
				result += "0";
			}
		}
		
		return result;
	}
	
	public static class Metrics {
		private Integer totalRuns;
		private Integer totalGuesses;
		private Double averageGuesses;
		private Integer expiredCount;
		private List<RunInfo> runs;
		
		public Metrics() {
			this.totalRuns = 0;
			this.totalGuesses = 0;
			this.averageGuesses = 0.0;
			this.expiredCount = 0;
			this.runs = new LinkedList<RunInfo>();
		}

		public void updateMetric(Integer guesses) {
			this.totalGuesses += guesses;
			
			this.averageGuesses = this.totalGuesses.doubleValue() / this.totalRuns.doubleValue();
		}
		
		public void initRun(String wordle) {
			this.totalRuns++;
			this.runs.add(new RunInfo(wordle));
		}
		
		public void incrementRun(String guess, String result) {
			this.runs.get(runs.size() - 1).enterGuess(guess, result);
		}
		
		public void expireRun() {
			this.runs.get(runs.size() - 1).expire();
			this.expiredCount++;
		}
		
		@Override
		public String toString() {
			String display = "Runs: " + totalRuns 
					+ "\t" + "Average Success: " + String.format("%.2f", averageGuesses)
					+ "\t" + "Failures: " + expiredCount
					+ "\t" + "Success %: " + String.format("%.2f", (totalRuns.doubleValue() - expiredCount.doubleValue()) / totalRuns.doubleValue())
					+ "\n\n";
			for (RunInfo run : this.runs) {
				display += run + "\n\n";
			};
			
			return display;
		}
	}
	
	public static class RunInfo {
		String wordle;
		Integer guessCount;
		List<String> guesses;
		List<String> results;
		Boolean expired;
		
		public RunInfo(String wordle) {
			this.wordle = wordle;
			this.guesses = new LinkedList<String>();
			this.results = new LinkedList<String>();
			this.guessCount = 0;
			this.expired = false;
		}
		
		public void enterGuess(String guess, String result) {
			this.guesses.add(guess);
			this.results.add(result);
			this.guessCount += 0;
		}
		
		public void expire() {
			this.expired = true;
		}
		
		@Override
		public String toString() {
			String output = this.wordle + (this.expired ? " [EXPIRED]" : " [" + this.guesses.size() + "]");
			
			for (int i = 0; i < guesses.size(); i++) {
				output += String.format("\n%5s(%d) %s | %s", "", i + 1, guesses.get(i), results.get(i));
			}
			
			return output;
		}
	}

}
