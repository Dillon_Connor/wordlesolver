package gui;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.*;

import solver.InteractiveSolver;

public class Main extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2100486903982807591L;
	private static final int APP_WIDTH = 400;
	private static final int APP_HEIGHT = 500;
	
	public Main() {
		setSize(APP_WIDTH, APP_HEIGHT);
		setTitle("Wordle Solver");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setMinimumSize(new Dimension(APP_WIDTH, APP_HEIGHT));
	}

	public static void main(String[] args) throws ParseException, IOException {
		Main gui = new Main();
		
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		
		JLabel title = new JLabel("Wordle Solver", JLabel.CENTER);
		title.setFont(new Font("Serif", Font.PLAIN, 26));
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		
		panel.add(title, constraints);
		
		constraints.gridy = 1;
		GuessPanel guessPanel = new GuessPanel();
		panel.add(guessPanel, constraints);
		
		JList<String> suggestionList = new JList<>(new DefaultListModel<String>());
		suggestionList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		SuggestionButton suggestionButton = new SuggestionButton(guessPanel, suggestionList);
		
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 5;
		constraints.gridx = 0;
		constraints.gridy = 7;
		panel.add(suggestionButton, constraints);

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 5;
		constraints.gridx = 0;
		constraints.gridy = 8;
		panel.add(suggestionList, constraints);
		
		constraints.gridy = 9;
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultListModel<String> listModel = (DefaultListModel<String>) suggestionList.getModel();
				listModel.removeAllElements();
				
				guessPanel.resetPanel();
				
				try {
					InteractiveSolver.initializePattern();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		panel.add(resetButton, constraints);

		gui.getRootPane().setDefaultButton(suggestionButton);
		
		gui.getContentPane().add(panel);
		gui.setVisible(true);
		
		guessPanel.placeFocus();
	}
}
