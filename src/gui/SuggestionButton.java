package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import solver.InteractiveSolver;

public class SuggestionButton extends JButton implements ActionListener, KeyListener, ListSelectionListener, FocusListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2765196684108754889L;
	private GuessPanel guessPanel;
	private JList<String> suggestionList;
	
	public SuggestionButton(GuessPanel guessPanel, JList<String> suggestionList) throws IOException {
		super("Get Suggestions");
		this.guessPanel = guessPanel;
		this.suggestionList = suggestionList;
		
		this.suggestionList.addListSelectionListener(this);
		
		this.addActionListener(this);
		this.addKeyListener(this);
		this.addFocusListener(this);
		
		// Set up the suggestion finder
		InteractiveSolver.initializePattern();
	}
	
	public void processSuggestions() throws IOException {
		if (!guessPanel.validRow()) return;
		String guess = guessPanel.getGuess();
		String result = guessPanel.getResult();
		
		List<String> suggestions = InteractiveSolver.getSuggestions(InteractiveSolver.getPatterns(guess, result));
		
		DefaultListModel<String> listModel = (DefaultListModel<String>) suggestionList.getModel();
		listModel.removeAllElements();
		for (String suggestion : suggestions) {
			listModel.addElement(suggestion);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			processSuggestions();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			try {
				processSuggestions();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		String selectedValue = this.suggestionList.getSelectedValue();
		if (e.getValueIsAdjusting() || selectedValue == null) return;
		
		guessPanel.enterSuggestion(selectedValue);
	}

	@Override
	public void focusGained(FocusEvent e) {
		guessPanel.placeFocus();
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}
}
