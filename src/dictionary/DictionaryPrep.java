package dictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

public class DictionaryPrep {
	private static Boolean PLURAL_REDUCTION = false;
	private static Map<String, Double[]> letterScoreMap;
	
	public static List<String> getWords() throws FileNotFoundException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("dict_opt.txt");
		Scanner scanner = new Scanner(is, "UTF-8");
		
		List<String> words = new LinkedList<String>();
		while (scanner.hasNextLine()) {
			words.add(scanner.nextLine());
		}
		
		scanner.close();

		Collections.reverse(words);
		return words;
	}
	
	public static List<DictionaryItem> getDictionaryItems() throws IOException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("dict_opt.txt");
		Scanner scanner = new Scanner(is, "UTF-8");
		letterScoreMap = null;
		
		List<DictionaryItem> items = new LinkedList<DictionaryItem>();
		while (scanner.hasNextLine()) {
			items.add(new DictionaryItem(scanner.nextLine()));
		}
		
		scanner.close();
		
		updateLetterScoreMap(items);
		updateDictionaryScores(items);
		Collections.reverse(items);
		return items;
	}

	public static void createFiveLetterOrderedDict() throws IOException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("dict_full.txt");
		Scanner scanner = new Scanner(is, "UTF-8");
		
		Set<String> removedWords = getRemovedWords();

		Pattern dpat = Pattern.compile("[a-zA-Z]{5}");
		List<DictionaryItem> items = new LinkedList<DictionaryItem>();
		while (scanner.hasNext()) {
			String word = scanner.next();
			if (!word.equals(word.toLowerCase())) continue;
			
			if (dpat.matcher(word).matches() && !removedWords.contains(word)) {
				items.add(new DictionaryItem(word));
			}
		}
		
		items.sort(null);
		FileOutputStream os = new FileOutputStream(new File("src/dict_opt.txt"));
		for (DictionaryItem item : items) {
			os.write((item.getWord() + "\n").getBytes());
		}
		
		is.close();
		os.close();
		scanner.close();
	}
	
	private static Set<String> getRemovedWords() throws FileNotFoundException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("removed.txt");
		Scanner scanner = new Scanner(is, "UTF-8");
		Set<String> removedWords = new HashSet<String>();
		
		while (scanner.hasNext()) {
			removedWords.add(scanner.next().toLowerCase());
		}
		
		scanner.close();
		return removedWords;
	}
	
	public static void updateLetterScoreMap(List<DictionaryItem> items) {
		letterScoreMap = new HashMap<>();
		
		Map<String, double[]> letterTotalsMap = new HashMap<String, double[]>();
		double totalLetterCount = 0;
		for (DictionaryItem item : items) {
			String word = item.getWord();
			
			for (int i = 0; i < 5; i++) {
				String letter = (word.charAt(i) + "").toLowerCase();
				
				// Don't over-value plurals
				if (PLURAL_REDUCTION && i == 4 && "s".equals(letter)) continue;
				
				double[] letterTotals = letterTotalsMap.get(letter);
				if (letterTotals == null) {
					letterTotals = new double[6];
					letterTotalsMap.put(letter, letterTotals);
				}
				
				totalLetterCount++;
				letterTotals[0]++;
				letterTotals[i + 1]++;
			}
		}
		
		for (String letter : letterTotalsMap.keySet()) {
			Double[] letterValues = new Double[6];
			letterScoreMap.put(letter, letterValues);
			
			// Position 0 - score for overall frequency
			// Positions 1-5 scores for frequency in order placement
			
			for (int i = 0; i < 6; i++) {
				double value = letterTotalsMap.get(letter)[i] / (totalLetterCount / (i > 0 ? 5 : 1)) * 100;
				
				letterValues[i] = value;
			}
		}
	}
	
	public static void updateDictionaryScores(List<DictionaryItem> items) {
		for (DictionaryItem item : items) {
			item.updateWordScore();
		}
	}

	/*
	private static boolean isVowel(String letter) {
		switch (letter) {
		case "a":
		case "e":
		case "i":
		case "o":
		case "u":
			return true;
		default:
			return false;
		}
	}
	*/
	
	public static void printLetterScores() {
		System.out.println("\nLetter Score Map");
		for (String letter : letterScoreMap.keySet()) {
			Double[] values = letterScoreMap.get(letter);
			
			System.out.println(String.format(
					"%s: %7.3f [%7.3f ] [%7.3f ] [%7.3f ] [%7.3f ] [%7.3f ]",
					letter,
					values[0],
					values[1],
					values[2],
					values[3],
					values[4],
					values[5]
				));
		}
		System.out.println();
	}

	public static class DictionaryItem implements Comparable<Object> {
		private String word;
		private Double score;
		
		public DictionaryItem(String word) throws IOException {
			this.word = word.toLowerCase();
			this.score = getWordScore(this.word);
		}

		public String getWord() {
			return this.word;
		}

		private Double getWordScore(String word) {
			if (letterScoreMap == null) return 0.0;
			
			Set<String> checkedLetters = new HashSet<>();
			Double score = 0.0;
			for (int i = 0; i < 5; i++) {
				String letter = word.substring(i, i + 1);
				Double[] letterValues = letterScoreMap.get(letter);
				
				if (letterValues == null) continue;
				Double letterScore = letterValues[0] + letterValues[i + 1];

				if (checkedLetters.contains(letter)) continue;
				score += letterScore;
				
				checkedLetters.add(letter);
			}
			
			return score;
		}
		
		private void updateWordScore() {
			this.score = getWordScore(this.word);
		}

		@Override
		public int compareTo(Object o) {
			DictionaryItem other = (DictionaryItem) o;
			
			if (other.score < this.score) {
				return -1;
			}
			
			if (other.score > this.score) {
				return 1;
			}
			return 0;
		}

		@Override
		public String toString() {
			return this.word + ": " + this.score;
		}
	}
}
