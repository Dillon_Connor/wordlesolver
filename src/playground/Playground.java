package playground;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import automated.WordleRunner;
import dictionary.DictionaryPrep;
import dictionary.DictionaryPrep.DictionaryItem;
import solver.InteractiveSolver;

public class Playground {

	public static void main(String[] args) throws IOException {
		// playOne();
		playTwo();
		// playThree();
		// playFour();
		// playFive();

		// DictionaryPrep.buildLetterScoreFile();
		// updateDictionary();
		// checkWordScores();
	}
	
	public static void playOne() throws IOException {
		String compWord = "syrup";
		
		InteractiveSolver.initializePattern();
		List<Pattern> patterns = InteractiveSolver.getPatterns("usurp", "11012");
		
		Boolean matches = true;
		for (Pattern p : patterns) {
			System.out.println(p.toString());
			if (!p.matcher(compWord).matches()) {
				matches = false;
				break;
			}
		}
		
		System.out.println(matches);
	}
	
	public static void playTwo() {
		List<String> checks = new LinkedList<String>();
		// Real example
		/*
		checks.add(WordleRunner.getResult("perky", "shine"));
		checks.add(WordleRunner.getResult("perky", "beard"));
		checks.add(WordleRunner.getResult("perky", "pervy"));
		checks.add(WordleRunner.getResult("perky", "perky"));
		*/
		
		// Some tricky cases
		// checks.add(WordleRunner.getResult("xylyl", "adieu"));
		checks.add(WordleRunner.getResult("xylyl", "hello"));
		checks.add(WordleRunner.getResult("allel", "allee"));
		
		for (String check : checks) {
			System.out.println(check);
		}
	}
	
	public static void playThree() throws IOException {
		WordleRunner.metrics = new WordleRunner.Metrics();
		WordleRunner.runWordle("adieu");
		System.out.println(WordleRunner.metrics);
	}
	
	public static void playFour() {
		WordleRunner.RunInfo info = new WordleRunner.RunInfo("could");
		info.enterGuess("shire", "00000");
		info.enterGuess("mount", "02200");
		info.enterGuess("would", "02222");
		info.enterGuess("could", "22222");
		
		System.out.println(info);
	}
	
	public static void playFive() {
		List<String> strings = new LinkedList<String>();
		
		strings.add("one");
		strings.add("two");
		strings.add("three");
		
		System.out.println(strings.subList(0, Math.min(4, strings.size())));
	}
	
	public static void checkWordScores() throws IOException {
		List<DictionaryPrep.DictionaryItem> items = new LinkedList<DictionaryPrep.DictionaryItem>();
		items.add(new DictionaryItem("child"));
		items.add(new DictionaryItem("adieu"));
		items.add(new DictionaryItem("knoll"));
		items.add(new DictionaryItem("juice"));
		items.add(new DictionaryItem("shine"));
		items.add(new DictionaryItem("qualm"));
		items.add(new DictionaryItem("orate"));
		items.add(new DictionaryItem("areae"));
		items.add(new DictionaryItem("immix"));
		items.add(new DictionaryItem("sared"));
		
		items.sort(null);
		
		for (DictionaryPrep.DictionaryItem di : items) {
			System.out.println(di);
		}
	}
	
	public static void updateDictionary() throws IOException {
		DictionaryPrep.createFiveLetterOrderedDict();
	}

}
